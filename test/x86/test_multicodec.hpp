/* SPDX-License-Identifier: GPL-3.0-or-later */

#ifndef TEST_X86_TEST_MULTICODEC_HPP_
#define TEST_X86_TEST_MULTICODEC_HPP_

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

class test_multicodec : public CppUnit::TestFixture
{
  CPPUNIT_TEST_SUITE(test_multicodec);
  CPPUNIT_TEST(test_state);
  CPPUNIT_TEST(test_different);
  CPPUNIT_TEST(test_raw);
  CPPUNIT_TEST(test_dag_pb);
  CPPUNIT_TEST(test_static_raw);
  CPPUNIT_TEST(test_static_dag_pb);
  CPPUNIT_TEST_SUITE_END();

public:
  void
  test_state();

  void
  test_different();

  void
  test_raw();

  void
  test_dag_pb();

  void
  test_static_raw();

  void
  test_static_dag_pb();
};

#endif /* TEST_X86_TEST_MULTICODEC_HPP_ */
