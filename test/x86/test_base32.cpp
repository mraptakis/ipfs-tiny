/* SPDX-License-Identifier: GPL-3.0-or-later */

#include <cppunit/TestCase.h>
#include <etl/string.h>
#include <etl/vector.h>
#include <iostream>
#include <random>

#include "ipfs-tiny/multiformats/base32.hpp"
#include "test_base32.hpp"

static void
string_to_vector_uint8(etl::istring &input, etl::ivector<uint8_t> &output)
{
  for (etl::istring::iterator it = input.begin(); it != input.end(); ++it)
    output.push_back((uint8_t)*it);
}

void
test_base32::enc()
{
  ipfs_tiny::multiformats::base32<etl::ivector<uint8_t>, etl::ivector<uint8_t>>
      b;

  // small example
  etl::string<6>          small("foobar");
  etl::vector<uint8_t, 6> small_vec;
  string_to_vector_uint8(small, small_vec);

  etl::vector<uint8_t, 16> small_enc;
  b.encode(small_enc, small_vec);
  etl::string<16> small_enc_str(small_enc.begin(), small_enc.end());
  etl::string<16> small_enc_result("MZXW6YTBOI======");
  CPPUNIT_ASSERT(!small_enc_str.compare(small_enc_result));

  // medium example
  etl::string<18>          medium("foobarfoobarfoobar");
  etl::vector<uint8_t, 18> medium_vec;
  string_to_vector_uint8(medium, medium_vec);

  etl::vector<uint8_t, 39> medium_enc;
  medium_enc.push_back('P');
  medium_enc.push_back('R');
  medium_enc.push_back('E');
  medium_enc.push_back('F');
  medium_enc.push_back('I');
  medium_enc.push_back('X');
  medium_enc.push_back('-');
  b.encode(medium_enc, medium_vec);
  etl::string<39> medium_enc_str(medium_enc.begin(), medium_enc.end());
  etl::string<39> medium_enc_result("PREFIX-MZXW6YTBOJTG633CMFZGM33PMJQXE===");
  CPPUNIT_ASSERT(!medium_enc_str.compare(medium_enc_result));

  // large example
  etl::string<36>          large("foobarfoobarfoobarfoobarfoobarfoobar");
  etl::vector<uint8_t, 36> large_vec;
  string_to_vector_uint8(large, large_vec);

  etl::vector<uint8_t, 64> large_enc;
  b.encode(large_enc, large_vec);
  etl::string<64> large_enc_str(large_enc.begin(), large_enc.end());
  etl::string<64> large_enc_result(
      "MZXW6YTBOJTG633CMFZGM33PMJQXEZTPN5RGC4TGN5XWEYLSMZXW6YTBOI======");
  CPPUNIT_ASSERT(!large_enc_str.compare(large_enc_result));
}

void
test_base32::enc_str()
{
  ipfs_tiny::multiformats::base32<etl::ivector<uint8_t>, etl::ivector<uint8_t>>
      b;

  // small example
  etl::string<6>          small("foobar");
  etl::vector<uint8_t, 6> small_vec;
  string_to_vector_uint8(small, small_vec);

  etl::string<16> small_enc_str;
  b.encode(small_enc_str, small_vec);
  etl::string<16> small_enc_result("MZXW6YTBOI======");
  CPPUNIT_ASSERT(!small_enc_str.compare(small_enc_result));

  // medium example
  etl::string<18>          medium("foobarfoobarfoobar");
  etl::vector<uint8_t, 18> medium_vec;
  string_to_vector_uint8(medium, medium_vec);

  etl::string<39> medium_enc_str;
  medium_enc_str.push_back('P');
  medium_enc_str.push_back('R');
  medium_enc_str.push_back('E');
  medium_enc_str.push_back('F');
  medium_enc_str.push_back('I');
  medium_enc_str.push_back('X');
  medium_enc_str.push_back('-');
  b.encode(medium_enc_str, medium_vec);
  etl::string<39> medium_enc_result("PREFIX-MZXW6YTBOJTG633CMFZGM33PMJQXE===");
  CPPUNIT_ASSERT(!medium_enc_str.compare(medium_enc_result));

  // large example
  etl::string<36>          large("foobarfoobarfoobarfoobarfoobarfoobar");
  etl::vector<uint8_t, 36> large_vec;
  string_to_vector_uint8(large, large_vec);

  etl::string<64> large_enc_str;
  b.encode(large_enc_str, large_vec);
  etl::string<64> large_enc_result(
      "MZXW6YTBOJTG633CMFZGM33PMJQXEZTPN5RGC4TGN5XWEYLSMZXW6YTBOI======");
  CPPUNIT_ASSERT(!large_enc_str.compare(large_enc_result));
}

void
test_base32::dec()
{
  ipfs_tiny::multiformats::base32<etl::ivector<uint8_t>, etl::ivector<uint8_t>>
      b;

  // small example
  etl::string<16>          small_enc_str("MZXW6YTBOI======");
  etl::vector<uint8_t, 16> small_enc_vec;
  string_to_vector_uint8(small_enc_str, small_enc_vec);

  etl::vector<uint8_t, 10> small_vec;
  b.decode(small_vec, small_enc_vec);
  etl::string<6> small(small_vec.begin(), small_vec.end());
  etl::string<6> small_result("foobar");
  CPPUNIT_ASSERT(!small.compare(small_result));

  // medium example
  etl::string<32>          medium_enc_str("MZXW6YTBOJTG633CMFZGM33PMJQXE===");
  etl::vector<uint8_t, 32> medium_enc_vec;
  string_to_vector_uint8(medium_enc_str, medium_enc_vec);

  etl::vector<uint8_t, 27> medium_vec;
  medium_vec.push_back('P');
  medium_vec.push_back('R');
  medium_vec.push_back('E');
  medium_vec.push_back('F');
  medium_vec.push_back('I');
  medium_vec.push_back('X');
  medium_vec.push_back('-');
  b.decode(medium_vec, medium_enc_vec);
  etl::string<25> medium(medium_vec.begin(), medium_vec.end());
  etl::string<25> medium_result("PREFIX-foobarfoobarfoobar");
  CPPUNIT_ASSERT(!medium.compare(medium_result));

  // large example
  etl::string<64> large_enc_str(
      "MZXW6YTBOJTG633CMFZGM33PMJQXEZTPN5RGC4TGN5XWEYLSMZXW6YTBOI======");
  etl::vector<uint8_t, 64> large_enc_vec;
  string_to_vector_uint8(large_enc_str, large_enc_vec);

  etl::vector<uint8_t, 40> large_vec;
  b.decode(large_vec, large_enc_vec);
  etl::string<36> large(large_vec.begin(), large_vec.end());
  etl::string<36> large_result("foobarfoobarfoobarfoobarfoobarfoobar");
  CPPUNIT_ASSERT(!large.compare(large_result));
}

void
test_base32::dec_str()
{
  ipfs_tiny::multiformats::base32<etl::ivector<uint8_t>, etl::ivector<uint8_t>>
      b;

  // small example
  etl::string<16> small_enc_str("MZXW6YTBOI======");

  etl::vector<uint8_t, 10> small_vec;
  b.decode(small_vec, small_enc_str);
  etl::string<6> small(small_vec.begin(), small_vec.end());
  etl::string<6> small_result("foobar");
  CPPUNIT_ASSERT(!small.compare(small_result));

  // medium example
  etl::string<32> medium_enc_str("MZXW6YTBOJTG633CMFZGM33PMJQXE===");

  etl::vector<uint8_t, 27> medium_vec;
  medium_vec.push_back('P');
  medium_vec.push_back('R');
  medium_vec.push_back('E');
  medium_vec.push_back('F');
  medium_vec.push_back('I');
  medium_vec.push_back('X');
  medium_vec.push_back('-');
  b.decode(medium_vec, medium_enc_str);
  etl::string<25> medium(medium_vec.begin(), medium_vec.end());
  etl::string<25> medium_result("PREFIX-foobarfoobarfoobar");
  CPPUNIT_ASSERT(!medium.compare(medium_result));

  // large example
  etl::string<64> large_enc_str(
      "MZXW6YTBOJTG633CMFZGM33PMJQXEZTPN5RGC4TGN5XWEYLSMZXW6YTBOI======");

  etl::vector<uint8_t, 40> large_vec;
  b.decode(large_vec, large_enc_str);
  etl::string<36> large(large_vec.begin(), large_vec.end());
  etl::string<36> large_result("foobarfoobarfoobarfoobarfoobarfoobar");
  CPPUNIT_ASSERT(!large.compare(large_result));
}

void
test_base32::full_random()
{
  std::random_device                     rd;
  std::mt19937                           mt(rd());
  std::uniform_int_distribution<uint8_t> uni(0, 255);

  const size_t msg_len = 1024;
  ipfs_tiny::multiformats::base32<etl::ivector<uint8_t>, etl::ivector<uint8_t>>
                                              b;
  etl::vector<uint8_t, msg_len + 1>           raw;
  etl::vector<uint8_t, msg_len + 1>           raw_output;
  etl::vector<uint8_t, (msg_len + 4) / 5 * 8> encoded;

  for (size_t i = 0; i < msg_len; i++) {
    raw.push_back(uni(mt));
  }

  b.encode(encoded, raw);
  b.decode(raw_output, encoded);
  CPPUNIT_ASSERT(etl::equal(raw.cbegin(), raw.cend(), raw_output.cbegin()));
}

void
test_base32::full_random_iter()
{
  std::random_device                     rd;
  std::mt19937                           mt(rd());
  std::uniform_int_distribution<uint8_t> uni(0, 255);

  const size_t msg_len = 1024;
  ipfs_tiny::multiformats::base32<etl::ivector<uint8_t>, etl::ivector<uint8_t>>
                                              b;
  etl::vector<uint8_t, msg_len + 1>           raw;
  etl::vector<uint8_t, msg_len + 1>           raw_output;
  etl::vector<uint8_t, (msg_len + 4) / 5 * 8> encoded;

  for (size_t i = 0; i < msg_len; i++) {
    raw.push_back(uni(mt));
  }

  b.encode(encoded, raw);
  b.decode(raw_output, encoded.cbegin(), encoded.cend());
  CPPUNIT_ASSERT(etl::equal(raw.cbegin(), raw.cend(), raw_output.cbegin()));
}

void
test_base32::full_random_str()
{
  std::random_device                     rd;
  std::mt19937                           mt(rd());
  std::uniform_int_distribution<uint8_t> uni(0, 255);

  const size_t msg_len = 1024;
  ipfs_tiny::multiformats::base32<etl::ivector<uint8_t>, etl::istring> b;
  etl::vector<uint8_t, msg_len + 1>                                    raw;
  etl::vector<uint8_t, msg_len + 1>  raw_output;
  etl::string<(msg_len + 4) / 5 * 8> encoded;

  for (size_t i = 0; i < msg_len; i++) {
    raw.push_back(uni(mt));
  }

  b.encode(encoded, raw);
  b.decode(raw_output, encoded);
  CPPUNIT_ASSERT(etl::equal(raw.cbegin(), raw.cend(), raw_output.cbegin()));
}
