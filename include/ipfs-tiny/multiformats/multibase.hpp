/* SPDX-License-Identifier: GPL-3.0-or-later */

#ifndef INCLUDE_IPFS_TINY_MULTIFORMATS_MULTIBASE_HPP_
#define INCLUDE_IPFS_TINY_MULTIFORMATS_MULTIBASE_HPP_

#include "base.hpp"
#include "base16.hpp"
#include "base32.hpp"
#include "base58.hpp"
#include "etl/map.h"
#include "ipfs-tiny/exception.hpp"
#include <type_traits>

namespace ipfs_tiny::multiformats::multibase
{

class uknown_base : public exception
{
public:
  uknown_base(const char *file, size_t line)
      : exception("multibase: Unknown base", file, line)
  {
  }
};

/**
 *
 */
enum class encoding : char
{
  extract   = 0,   /**< Search for the encoding. Valid only during decoding */
  base16    = 'f', /**< base16 */
  base32    = 'b', /**< base32 */
  base58btc = 'z'  /**< base58btc */
};

template <const encoding T, typename EncodedT, typename DecodedT>
static EncodedT &
encode(EncodedT &res, const DecodedT &data)
{
  if constexpr (T == encoding::base16) {
    base16<DecodedT, EncodedT> b16;
    etl::string<1>             c;
    c.push_back(b16.code());
    res.append(c);
    return b16.encode(res, data);
  } else if constexpr (T == encoding::base32) {
    base32<DecodedT, EncodedT> b32;
    etl::string<1>             c;
    c.push_back(b32.code());
    res.append(c);
    return b32.encode(res, data);
  } else if constexpr (T == encoding::base58btc) {
    base58<DecodedT, EncodedT> b58;
    etl::string<1>             c;
    c.push_back(b58.code());
    res.append(c);
    return b58.encode(res, data);
  } else {
    throw uknown_base(__FILE__, __LINE__);
  }
}

template <typename EncodedT, typename DecodedT>
static EncodedT &
encode(encoding enc, EncodedT &res, const DecodedT &data)
{
  switch (enc) {
  case encoding::base16:
    return encode<encoding::base16>(res, data);
  case encoding::base32:
    return encode<encoding::base32>(res, data);
  case encoding::base58btc:
    return encode<encoding::base58btc>(res, data);
  default:
    throw uknown_base(__FILE__, __LINE__);
  }
}

template <typename DecodedT, typename EncodedT>
static DecodedT &
decode(DecodedT &res, typename EncodedT::const_iterator first,
       typename EncodedT::const_iterator last)
{
  if (first == last) {
    return res;
  }
  switch (static_cast<encoding>(*first++)) {
  case encoding::base16: {
    ipfs_tiny::multiformats::base16<DecodedT, EncodedT> b;
    b.decode(res, first, last);
    return res;
  }
  case encoding::base32: {
    ipfs_tiny::multiformats::base32<DecodedT, EncodedT> b;
    b.decode(res, first, last);
    return res;
  }
  case encoding::base58btc: {
    ipfs_tiny::multiformats::base58<DecodedT, EncodedT> b;
    b.decode(res, first, last);
    return res;
  }
  default:
    throw uknown_base(__FILE__, __LINE__);
  }
}

template <typename DecodedT, typename EncodedT>
static DecodedT &
decode(DecodedT &res, encoding &enc, typename EncodedT::const_iterator first,
       typename EncodedT::const_iterator last)
{
  if (first == last) {
    return res;
  }
  switch (static_cast<encoding>(*first++)) {
  case encoding::base16: {
    ipfs_tiny::multiformats::base16<DecodedT, EncodedT> b;
    enc = encoding::base16;
    b.decode(res, first, last);
    return res;
  }
  case encoding::base32: {
    ipfs_tiny::multiformats::base32<DecodedT, EncodedT> b;
    enc = encoding::base32;
    b.decode(res, first, last);
    return res;
  }
  case encoding::base58btc: {
    ipfs_tiny::multiformats::base58<DecodedT, EncodedT> b;
    enc = encoding::base58btc;
    b.decode(res, first, last);
    return res;
  }
  default:
    throw uknown_base(__FILE__, __LINE__);
  }
}

template <const encoding T, typename DecodedT, typename EncodedT>
static DecodedT &
decode(DecodedT &res, typename EncodedT::const_iterator first,
       typename EncodedT::const_iterator last)
{

  if (T == encoding::extract) {
    return decode<DecodedT, EncodedT>(res, first, last);
  }

  if (first == last) {
    return res;
  }

  switch (T) {
  case encoding::base16: {
    ipfs_tiny::multiformats::base16<DecodedT, EncodedT> b;
    b.decode(res, first, last);
    return res;
  }
  case encoding::base32: {
    ipfs_tiny::multiformats::base32<DecodedT, EncodedT> b;
    b.decode(res, first, last);
    return res;
  }
  case encoding::base58btc: {
    ipfs_tiny::multiformats::base58<DecodedT, EncodedT> b;
    b.decode(res, first, last);
    return res;
  }
  default:
    throw uknown_base(__FILE__, __LINE__);
  }
}

/**
 * Returns the base of a possibly multibase encoded stream
 * @tparam EncodedT iteratable container with the multibase encoded stream
 * @param first iterator to the first element that is multibase encoded
 * @throw uknown_base if the base cannot be identified
 * @return the multibase base
 */
template <typename EncodedT>
static encoding
get_base(typename EncodedT::const_iterator first)
{
  switch (static_cast<encoding>(*first)) {
  case encoding::base16: {
    return encoding::base16;
  }
  case encoding::base32: {
    return encoding::base32;
  }
  case encoding::base58btc: {
    return encoding::base58btc;
  }
  default:
    throw uknown_base(__FILE__, __LINE__);
  }
}

} // namespace ipfs_tiny::multiformats::multibase

#endif /* INCLUDE_IPFS_TINY_MULTIFORMATS_MULTIBASE_HPP_ */
