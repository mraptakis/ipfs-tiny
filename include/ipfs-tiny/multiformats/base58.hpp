/* SPDX-License-Identifier: GPL-3.0-or-later */

#ifndef INCLUDE_IPFS_TINY_CRYPTO_BASE58_HPP
#define INCLUDE_IPFS_TINY_CRYPTO_BASE58_HPP

#include "base.hpp"
#include "etl/format_spec.h"
#include <cstddef>
#include <etl/map.h>
#include <etl/string.h>
#include <etl/vector.h>

namespace ipfs_tiny::multiformats
{

class base58_decode_exception : public exception
{
public:
  base58_decode_exception(const char *file, size_t line)
      : exception("base58 decoding error", file, line)
  {
  }
};

/**
 * base58 (multibase code: f) class.
 *
 * @tparam T1 input data datatype
 * @tparam T2 output data datatype
 */
template <typename T1, typename T2> class base58 : public base<T1, T2>
{
private:
  static constexpr char b58digits_ordered[] =
      "123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz";

  class b58digits_map
  {
  public:
    constexpr b58digits_map()
    {
      for (size_t i = 0; i < 58; i++) {
        map[b58digits_ordered[i]] = i;
      }
    }
    constexpr uint8_t
    at(uint8_t i)
    {
      return map.at(i);
    }

  private:
    etl::map<uint8_t, uint8_t, 58> map;
  };

public:
  base58() : base<T1, T2>(etl::make_string("base58btc"), 'z') {}

  etl::ivector<uint8_t> &
  encode(etl::ivector<uint8_t> &res, const etl::ivector<uint8_t> &data)
  {
    size_t  previous_size = res.size();
    size_t  datasz        = data.size();
    size_t  b58sz         = res.max_size();
    int     carry;
    ssize_t i, j, high, zcount = 0;
    size_t  size;
    size_t  res_size = 0;

    while (zcount < (ssize_t)datasz && !data[zcount])
      ++zcount;

    size = base58::encode_size(datasz - zcount);

    res.resize(previous_size + size);

    for (i = zcount, high = size - 1; i < (ssize_t)datasz; ++i, high = j) {
      for (carry = data[i], j = size - 1; (j > high) || carry; --j) {
        carry += 256 * res[previous_size + j];
        res[previous_size + j] = carry % 58;
        carry /= 58;
      }
    }

    for (j = 0; j < (ssize_t)size && !res[previous_size + j]; ++j)
      ;

    if (b58sz <= zcount + size - j) {
      b58sz = zcount + size - j + 1;
      return res;
    }

    for (i = zcount; j < (ssize_t)size; ++i, ++j) {
      res_size++;
      res[previous_size + i] = b58digits_ordered[res[previous_size + j]];
    }
    if (res[0] == 0 && res.size() == 47) {
      res.erase(res.begin());
    } else {
      res.resize(previous_size + res_size);
    }

    return res;
  }

  etl::ivector<uint8_t> &
  encode(etl::ivector<uint8_t> &               res,
         etl::ivector<uint8_t>::const_iterator begin,
         etl::ivector<uint8_t>::const_iterator end)
  {
    size_t  previous_size = res.size();
    size_t  datasz        = end - begin;
    size_t  b58sz         = res.max_size();
    int     carry;
    ssize_t i, j, high, zcount = 0;
    size_t  size;
    size_t  res_size = 0;

    while (zcount < (ssize_t)datasz && !(*(begin + zcount)))
      ++zcount;

    size = base58::encode_size(datasz - zcount);

    res.resize(previous_size + size);

    for (i = zcount, high = size - 1; i < (ssize_t)datasz; ++i, high = j) {
      for (carry = *(begin + i), j = size - 1; (j > high) || carry; --j) {
        carry += 256 * res[previous_size + j];
        res[previous_size + j] = carry % 58;
        carry /= 58;
      }
    }

    for (j = 0; j < (ssize_t)size && !res[previous_size + j]; ++j)
      ;

    if (b58sz <= zcount + size - j) {
      b58sz = zcount + size - j + 1;
      return res;
    }

    for (i = zcount; j < (ssize_t)size; ++i, ++j) {
      res_size++;
      res[previous_size + i] = b58digits_ordered[res[previous_size + j]];
    }
    if (res[0] == 0 && res.size() == 47) {
      res.erase(res.begin());
    } else {
      res.resize(previous_size + res_size);
    }

    return res;
  }

  etl::istring &
  encode(etl::istring &res, const etl::ivector<uint8_t> &data)
  {
    size_t  previous_size = res.size();
    size_t  datasz        = data.size();
    size_t  b58sz         = res.max_size();
    int     carry;
    ssize_t i, j, high, zcount = 0;
    size_t  size;
    size_t  res_size = 0;

    while (zcount < (ssize_t)datasz && !data[zcount])
      ++zcount;

    size = base58::encode_size(datasz - zcount);

    res.resize(previous_size + size);

    for (i = zcount, high = size - 1; i < (ssize_t)datasz; ++i, high = j) {
      for (carry = data[i], j = size - 1; (j > high) || carry; --j) {
        carry += 256 * res[previous_size + j];
        res[previous_size + j] = carry % 58;
        carry /= 58;
      }
    }

    for (j = 0; j < (ssize_t)size && !res[previous_size + j]; ++j)
      ;

    if (b58sz <= zcount + size - j) {
      b58sz = zcount + size - j + 1;
      return res;
    }

    for (i = zcount; j < (ssize_t)size; ++i, ++j) {
      res_size++;
      res[previous_size + i] = b58digits_ordered[res[previous_size + j]];
    }
    if (res[0] == 0 && res.size() == 47) {
      res.erase(0, 1);
    } else {
      res.resize(previous_size + res_size);
    }
    return res;
  }

  etl::istring &
  encode(etl::istring &res, etl::ivector<uint8_t>::const_iterator begin,
         etl::ivector<uint8_t>::const_iterator end)
  {
    size_t  previous_size = res.size();
    size_t  datasz        = end - begin;
    size_t  b58sz         = res.max_size();
    int     carry;
    ssize_t i, j, high, zcount = 0;
    size_t  size;
    size_t  res_size = 0;

    while (zcount < (ssize_t)datasz && !(*(begin + zcount)))
      ++zcount;

    size = base58::encode_size(datasz - zcount);

    res.resize(previous_size + size);

    for (i = zcount, high = size - 1; i < (ssize_t)datasz; ++i, high = j) {
      for (carry = *(begin + i), j = size - 1; (j > high) || carry; --j) {
        carry += 256 * res[previous_size + j];
        res[previous_size + j] = carry % 58;
        carry /= 58;
      }
    }

    for (j = 0; j < (ssize_t)size && !res[previous_size + j]; ++j)
      ;

    if (b58sz <= zcount + size - j) {
      b58sz = zcount + size - j + 1;
      return res;
    }

    for (i = zcount; j < (ssize_t)size; ++i, ++j) {
      res_size++;
      res[previous_size + i] = b58digits_ordered[res[previous_size + j]];
    }
    if (res[0] == 0 && res.size() == 47) {
      res.erase(0, 1);
    } else {
      res.resize(previous_size + res_size);
    }

    return res;
  }

  etl::ivector<uint8_t> &
  decode(etl::ivector<uint8_t> &res, const etl::ivector<uint8_t> &data)
  {
    size_t len           = data.size();
    size_t previous_size = res.size();
    res.push_back(0);
    int resultlen = 1;
    try {
      for (int i = 0; i < len; i++) {
        uint16_t carry = b58digits_map().at(data[i]);
        for (int j = 0; j < resultlen; j++) {
          carry += (uint16_t)(res[previous_size + j]) * 58;
          res[previous_size + j] = (uint8_t)(carry & 0xff);
          carry >>= 8;
        }
        while (carry > 0) {
          res.push_back((uint16_t)(carry & 0xff));
          resultlen++;
          carry >>= 8;
        }
      }

      for (int i = 0; i < len && data[i] == '1'; i++) {
        res.push_back(0);
        resultlen++;
      }

      for (int i = resultlen - 1, z = (resultlen >> 1) + (resultlen & 1);
           i >= z; i--) {
        int k                  = res[previous_size + i];
        res[previous_size + i] = res[previous_size + resultlen - i - 1];
        res[previous_size + resultlen - i - 1] = k;
      }
    } catch (etl::map_out_of_bounds &e) {
      res.clear();
      throw base58_decode_exception(__FILE__, __LINE__);
    }
    return res;
  }

  etl::ivector<uint8_t> &
  decode(etl::ivector<uint8_t> &res, const etl::istring &data)
  {
    size_t len           = data.size();
    size_t previous_size = res.size();
    res.push_back(0);
    int resultlen = 1;
    try {
      for (int i = 0; i < len; i++) {
        uint16_t carry = b58digits_map().at(data[i]);
        for (int j = 0; j < resultlen; j++) {
          carry += (uint16_t)(res[previous_size + j]) * 58;
          res[previous_size + j] = (uint8_t)(carry & 0xff);
          carry >>= 8;
        }
        while (carry > 0) {
          res.push_back((uint16_t)(carry & 0xff));
          resultlen++;
          carry >>= 8;
        }
      }

      for (int i = 0; i < len && data[i] == '1'; i++) {
        res.push_back(0);
        resultlen++;
      }

      for (int i = resultlen - 1, z = (resultlen >> 1) + (resultlen & 1);
           i >= z; i--) {
        int k                  = res[previous_size + i];
        res[previous_size + i] = res[previous_size + resultlen - i - 1];
        res[previous_size + resultlen - i - 1] = k;
      }
    } catch (etl::map_out_of_bounds &e) {
      res.clear();
      throw base58_decode_exception(__FILE__, __LINE__);
    }
    return res;
  }

  /**
   * Decodes a range of data in the range of [\p begin, \p end)
   * @param res the structure to append the result
   * @param begin iterator to the first input data
   * @param end iterator to the last input data
   * @return reference to \p res
   */
  etl::ivector<uint8_t> &
  decode(etl::ivector<uint8_t> &res, etl::istring::const_iterator begin,
         etl::istring::const_iterator end)
  {
    size_t len           = end - begin;
    size_t previous_size = res.size();
    res.push_back(0);
    int resultlen = 1;
    try {
      for (int i = 0; i < len; i++) {
        uint16_t carry = b58digits_map().at(*(begin + i));
        for (int j = 0; j < resultlen; j++) {
          carry += (uint16_t)(res[previous_size + j]) * 58;
          res[previous_size + j] = (uint8_t)(carry & 0xff);
          carry >>= 8;
        }
        while (carry > 0) {
          res.push_back((uint16_t)(carry & 0xff));
          resultlen++;
          carry >>= 8;
        }
      }

      for (int i = 0; i < len && (*(begin + i) == '1'); i++) {
        res.push_back(0);
        resultlen++;
      }

      for (int i = resultlen - 1, z = (resultlen >> 1) + (resultlen & 1);
           i >= z; i--) {
        int k                  = res[previous_size + i];
        res[previous_size + i] = res[previous_size + resultlen - i - 1];
        res[previous_size + resultlen - i - 1] = k;
      }
    } catch (etl::map_out_of_bounds &e) {
      res.clear();
      throw base58_decode_exception(__FILE__, __LINE__);
    }
    return res;
  }

  /**
   * Decodes a range of data in the range of [\p begin, \p end)
   * @param res the structure to append the result
   * @param begin iterator to the first input data
   * @param end iterator to the last input data
   * @return reference to \p res
   */
  etl::ivector<uint8_t> &
  decode(etl::ivector<uint8_t> &               res,
         etl::ivector<uint8_t>::const_iterator begin,
         etl::ivector<uint8_t>::const_iterator end)
  {
    size_t len           = end - begin;
    size_t previous_size = res.size();
    res.push_back(0);
    int resultlen = 1;
    try {
      for (int i = 0; i < len; i++) {
        uint16_t carry = b58digits_map().at(*(begin + i));
        for (int j = 0; j < resultlen; j++) {
          carry += (uint16_t)(res[previous_size + j]) * 58;
          res[previous_size + j] = (uint8_t)(carry & 0xff);
          carry >>= 8;
        }
        while (carry > 0) {
          res.push_back((uint16_t)(carry & 0xff));
          resultlen++;
          carry >>= 8;
        }
      }

      for (int i = 0; i < len && (*(begin + i) == '1'); i++) {
        res.push_back(0);
        resultlen++;
      }

      for (int i = resultlen - 1, z = (resultlen >> 1) + (resultlen & 1);
           i >= z; i--) {
        int k                  = res[previous_size + i];
        res[previous_size + i] = res[previous_size + resultlen - i - 1];
        res[previous_size + resultlen - i - 1] = k;
      }
    } catch (etl::map_out_of_bounds &e) {
      res.clear();
      throw base58_decode_exception(__FILE__, __LINE__);
    }
    return res;
  }

  size_t
  encode_size(size_t input_size) const
  {
    return (input_size)*138 / 100 + 1;
  }
  size_t
  decode_size(size_t input_size) const
  {
    return (input_size)*100 / 138 + 1;
  }
};

} // namespace ipfs_tiny::multiformats

#endif /* INCLUDE_IPFS_TINY_CRYPTO_BASE58_HPP */
