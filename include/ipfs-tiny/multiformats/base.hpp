/* SPDX-License-Identifier: GPL-3.0-or-later */

#ifndef INCLUDE_IPFS_TINY_MULTIBASE_BASE_HPP_
#define INCLUDE_IPFS_TINY_MULTIBASE_BASE_HPP_

#include "etl/string.h"
#include "ipfs-tiny/exception.hpp"

namespace ipfs_tiny::multiformats
{

/**
 * The base() class is an abstract class that defines the prototypes for
 * all possible bases that can be implemented in the multibase protocol.
 *
 * Due to the possible input/output data types, the abstract class and the
 * derived are using template metaprogramming to support all possible
 * variations.
 *
 * @tparam T1 input data datatype
 * @tparam T2 output data datatype
 */
template <typename T1, typename T2> class base
{
public:
  base(const etl::istring &name, char code) : m_name(name), m_code(code) {}

  const etl::istring &
  name() const
  {
    return m_name;
  }

  char
  code() const
  {
    return m_code;
  }

  virtual bool
  operator==(const base &cmp)
  {
    if (typeid(*this) != typeid(cmp)) {
      return false;
    }
    return (code() == cmp.code()) && (name() == cmp.name());
  }

  /**
   * Encodes \p data
   * @param res the structure to append the result
   * @param data the input data
   * @return reference to \p res
   */
  virtual T2 &
  encode(T2 &res, const T1 &data) = 0;

  /**
   * Encodes data in the range of [\p begin, \p end)
   * @param res the structure to append the result
   * @param begin iterator to the first input data
   * @param end iterator to the last input data
   * @return reference to \p res
   */
  virtual T2 &
  encode(T2 &res, typename T1::const_iterator begin,
         typename T1::const_iterator end) = 0;

  /**
   * Decodes \p data
   * @param res the structure to append the result
   * @param data the input data
   * @return reference to \p res
   */
  virtual T1 &
  decode(T1 &res, const T2 &data) = 0;

  /**
   * Decodes a range of data in the range of [\p begin, \p end)
   * @param res the structure to append the result
   * @param begin iterator to the first input data
   * @param end iterator to the last input data
   * @return reference to \p res
   */
  virtual T1 &
  decode(T1 &res, typename T2::const_iterator begin,
         typename T2::const_iterator end) = 0;

  /**
   * Get the size of the encoded data, if \p len input items are given to the
   * encode() method. If the size depends on the actual input data, a
   * maximum upper limit is returned
   * @param len the size of the input data
   * @return the number of the encoded bytes in bytes given a input of
   * \p len
   */
  virtual size_t
  encode_size(size_t len) const = 0;

  /**
   * Get the size of the decoded data, if \p len input items are given to the
   * decode() method. If the size depends on the actual input data, a
   * maximum upper limit is returned
   * @param len the size of the input data
   * @return the number of the decoded bytes in bytes given a input of
   * \p len
   */
  virtual size_t
  decode_size(size_t len) const = 0;

protected:
  const etl::string<32> m_name;
  const char            m_code;
};

} // namespace ipfs_tiny::multiformats

#endif /* INCLUDE_IPFS_TINY_MULTIBASE_BASE_HPP_ */
